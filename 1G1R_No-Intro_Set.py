#!/usr/bin/env python
# coding: utf-8
#by untamed yeti

import glob
import os
import shutil
from os.path import join
import pandas as pd
import numpy as np


# # Define Region List

region_names = [
#World
'(World)', 
#USA
'(USA)','(USA, Europe)','(USA, Europe, Brazil)','(USA, Europe, Korea)','(USA, Australia)','(USA, Asia)', '(USA, Korea)',
'(USA, Brazil)','(USA, Asia, Korea)',
#Europe
'(Europe)','(Europe, Australia)','(Europe, Canada)','(Europe, Brazil)','(Europe, Asia)','(Europe, Korea)',
#Austrilia
'(Australia)',
#Canada
'(Canada)', 
#Japan
'(Japan, USA)','(Japan, USA, Korea)','(Japan, USA, Brazil)','(Japan, Europe)','(Japan, Europe, Brazil)','(Japan)',
'(Japan, Korea)','(Japan, Asia, Korea)','(Japan, Hong Kong)','(Japan, Brazil)',
#Germany 
'(Germany)',
#Russia
'(Russia)',
#Asia
'(Asia)','(Asia, Korea)',
#Hongkong
'(Hong Kong)', 
#Tawian
'(Taiwan)', 
#Korea
'(Korea)',
#China
'(China)', 
#Spain
'(Spain)', 
#Italy
'(Italy)',
#Mexico
'(Mexico)', 
#Brazil
'(Brazil)','(Brazil, Portugal)',
#France
'(France)', 
#Netherlands
'(Netherlands)',
#Norway
'(Norway)',    
#Denmark
'(Denmark)',   
#Sweden
'(Sweden)', 
#Finland
'(Finland)'  
]

# # This Loops Through Selected Folder


#do one folder at a time
path = r'F:\LaunchBox\Games\system' #update your file system
data = []
files = os.listdir(path)
for name in files:
    data.append(name)
df = pd.DataFrame(data, columns=['Filename'])
df['Keep'] = np.nan


# # Clean Data Frame

df['GameName'] = df['Filename'].str.split("(", n = 1, expand = True)[0]
df['Country'] = '('+df['Filename'].str.split("(", n = 1, expand = True)[1]
df['Country'] = df['Country'].str.rstrip('.zip')
df['Country'] = df['Country'].str.rstrip('.rom')
df['GameName'] = df['GameName'].str.strip()
df['Country'] = df['Country'].str.strip()

# # Remove Unwanted Games (1st Pass)

#remove unwanted games
for index, game in df.iterrows():
    try:
        if '[BIOS]' in game['GameName'] or 'Pirate' in game['Country'] or 'Proto' in game['Country']         or 'Beta' in game['Country'] or '(Unl)' in game['Country'] or 'Demo' in game['Country']         or 'Program' in game['Country'] or 'Promo' in game['Country'] or 'Unknown' in game['Country']         or 'Debug' in game['Country'] or 'Diskmag' in game['Country'] or 'Debug'in game['Country']         or 'Software' in game['Country'] or 'Coverdisk' in game['Country'] or 'Addon' in game['Country']         or 'Probably' in game['Country'] or 'Enhancement Chip' in game['Country'] or 'Virus' in game['Country']        or 'Sample' in game['Country'] or 'Preview' in game['Country'] or 'Kiosk' in game['Country']         or 'Firmware' in game['Country'] or 'Manual' in game['Country'] or 'Cheat' in game['Country']         or 'virus' in game['Country'] or 'Unreleased' in game['Country'] or 'Trailer' in game['Country']:
            df.drop(index, inplace=True)
    except:
        print (game['GameName'])

# # LowerCase Game Name

df['GameName'] = df['GameName'].str.lower()
df.head()

# # Remove Duplicates by Region

#Remove duplicate region games
title = []
for region in region_names:
    #find games we want to keep
    for index, game in df.iterrows():
        #check if game country is in region and not already added to title
        if (game['Country']) == region and game['GameName'] not in title:
            df.loc[index,'Keep'] = True
            title.append(game['GameName'])
    #if game is in list (already found) then it will delte other country games
    for index, game in df.iterrows():
        if game['GameName'] in title and game['Keep'] != True:
            df.drop(index, inplace=True)

# # Keep Expanded Games

#Expand columns
df['Country2'] = df['Country'].str.split(pat=(')'), n=1, expand=True)[1]
df['Country3'] = df['Country'].str.split(pat=(')'), n=1, expand=True)[0] + ')'
df['GameName'] = df['GameName'] + df['Country2']
for region in region_names:
    #find games we want to keep
    for index, game in df.iterrows():
        #check if game country is in region and not already added to title
        if (game['Country3']) == region and game['GameName'] not in title:
            df.loc[index,'Keep'] = True
            title.append(game['GameName'])    
    #if game is in list (already found) then it will delte other country games
    for index, game in df.iterrows():
        if game['GameName'] in title and game['Keep'] != True:
            df.drop(index, inplace=True)

# # Keep Unique Games

#Keep Unique NANs
df['count'] = df.groupby('GameName')['GameName'].transform('count')
df.loc[df['count'] == 1, 'Keep'] = True
#Add Game to list
for index, game in df.iterrows():
    if game['GameName'] not in title and game['Keep'] == True:
        title.append(game['GameName'])
 #if game is in list (already found) then it will delte other country games
for index, game in df.iterrows():
    if game['GameName'] in title and game['Keep'] != True:
        df.drop(index, inplace=True)

# # Move Uneeded Files
#(replace XXXXX with your user name)
if not os.path.exists(r'C:\Users\XXXXX\Desktop\not_needed'): 
    os.mkdir(r'C:\Users\XXXXX\Desktop\not_needed')

#put wanted filename to a list. (replace XXXXX with your user name)
dest = r'C:\Users\XXXXX\Desktop\not_needed'
titles = df['Filename'].to_list()

#Moving Files that are not needed
for name in files:
    if name not in titles:
        print ('moving to: '+ os.path.join(dest, name))
        shutil.move(os.path.join(path, name), os.path.join(dest, name))

